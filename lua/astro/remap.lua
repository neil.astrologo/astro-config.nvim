vim.g.mapleader = " "
vim.keymap.set("n", "<leader>pv", vim.cmd.Ex)

-- Kemaps for enclosing words
vim.keymap.set("n", "<leader>q'", "ciW''<Esc>P")
vim.keymap.set("n", "<leader>q\"", "ciW\"\"<Esc>P")
vim.keymap.set("n", "<leader>q(", "ciW()<Esc>P")
vim.keymap.set("n", "<leader>q{", "ciW{}<Esc>P")
vim.keymap.set("n", "<leader>q[", "ciW[]<Esc>P")
vim.keymap.set("n", "<leader>q`", "ciW``<Esc>P")
vim.keymap.set("n", "<leader>q<", "ciW<><Esc>P")

vim.keymap.set("v", "<leader>q'", "c'<C-r>\"'<Esc>")
vim.keymap.set("v", "<leader>q\"", "c\"<C-r>\"\"<Esc>")
vim.keymap.set("v", "<leader>q(", "c(<C-r>\")<Esc>")
vim.keymap.set("v", "<leader>q{", "c{<C-r>\"}<Esc>")
vim.keymap.set("v", "<leader>q[", "c[<C-r>\"]<Esc>")
vim.keymap.set("v", "<leader>q`", "c`<C-r>\"`<Esc>")
vim.keymap.set("v", "<leader>q<", "c<<C-r>\"><Esc>")

-- Keymaps for quick moving between splits
vim.keymap.set("n", "<A-h>", "<C-w>h")
vim.keymap.set("n", "<A-j>", "<C-w>j")
vim.keymap.set("n", "<A-k>", "<C-w>k")
vim.keymap.set("n", "<A-l>", "<C-w>l")

vim.keymap.set("n", "<leader><NL>", "0i<CR><Esc>")
vim.keymap.set("n", "<leader>g<NL>", "0i<CR><Esc>k")
